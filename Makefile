CC=gcc
CLAGS=-Wall
LDLIBS=-lm

9: 9.o klient.o
	$(CC) $(CFLAGS) -o 9 9.o klient.o $(LDLIBS)

9.o: 9.c moduly.h
	$(CC) $(CFLAGS) -c 9.c

klient.o: klient.c
	$(CC) $(CFLAGS) -c klient.c