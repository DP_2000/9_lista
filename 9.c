/* Stwórz program w pliku .c, w którym będzie main() i który będzie miał załączony plik
moduly.h. Program ma być w stanie obsłużyć:
• dodanie klienta (1 lub więcej naraz, użytkownik wskazuje ile i podaje nazwiska)
• usunięcie klienta o danym nazwisku (uzupełnić pop by surname() w module)
• wyświetlenie ilości klientów
• wyświetlenie nazwisk klientów*/

#include <stdlib.h> 
#include <stdio.h>
#include <string.h>
#include "moduly.h"

int main()
{
    struct Client *myHead;
    myHead=(struct Client*)malloc(sizeof(struct Client));
    myHead = NULL;

    //dodanie klienta (1 lub więcej naraz, użytkownik wskazuje ile i podaje nazwiska)
    int n = 0, i = 1;
    char mySurname[40] = "";
    int option = 0;

    printf("Number of clients to add: \n");
    scanf("%d", &n);

    while(n>0)
    {
        do{
            printf("To add %d. client at the beggining of the list choose 1, at the end choose 2 and in the middle choose 3. \n", i);
            scanf("%d", &option);
        }while(option < 1 || option >3);


        if(option==1)
        {
            printf("Client's surname: \n");
            scanf("%s", mySurname); 
            push_front(&myHead, mySurname);
        }

        else if(option==2)
        {
            printf("Client's surname: \n");
            scanf("%s", mySurname); 
            push_back(&myHead, mySurname);
        
        }

        else if(option==3)
        {
            int index = 0;
            printf("Client's surname: \n");
            scanf("%s", mySurname); 
            printf("Client's index:");
            scanf("%d", &index);
            push_by_index(&myHead, mySurname, index);
        }

        n--;
        i++;
    }

    //usunięcie klienta o danym nazwisku (uzupełnić pop by surname() w module)
    printf("Client's surname to delete:\n");
    scanf("%s", mySurname);
    pop_by_surname(&myHead, mySurname);
    
    //wyświetlenie ilości klientów
    printf("\nNumber of clients: %d\n", list_size(myHead));

    //wyświetlenie nazwisk klientów
    show_list(myHead);

    return 0;
}
